package steps;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import pom.GooglePage;

public class GoogleSteps {
    GooglePage page = new GooglePage();

    @Dado("^eu acesso a homepage do google$")
    public void euAcessoAHomepageDoGoogle() {
        page.acess("https://www.google.com/");
    }

    @Quando("^pesquiso por \"([^\"]*)\"$")
    public void pesquisoPor(String keywork) throws Throwable {
        page.searchKey(keywork);
    }

    @Entao("^a URL \"([^\"]*)\" deve estar visivel$")
    public void aURLDeveEstarVisivel(String dockerUrl) throws Throwable {
        Assert.assertEquals(dockerUrl, page.checkUrl());
    }
}
